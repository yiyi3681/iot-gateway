package apps.labs.module07;

import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;

/**
 * This class contains the main method to build a Coap server, 
 * including initiates a CoAP object and adds resource to it
 */
public class CoapServerConnector {
	//required parameters
	private static final Logger _Logger =Logger.getLogger(CoapServerConnector.class.getName());
	private CoapServer _coapServer;
		
	/**
	 * Default constructor
	 */
	public CoapServerConnector()
	{
		super();
	}
		
	// public methods
	/**
	 * Add a resource to the coap server
	 * @param resource
	 */
	public void addResource(CoapResource resource)
	{
		if (resource != null)
			_coapServer.add(resource);
	}

	/**
	 * Instantiate a CoapServer and add a TempResourceHandler to it 
	 */
	public void start()
	{
		if (_coapServer == null) 
		{
			_Logger.info("Creating CoAP server instance and 'temp' handler...");
			_coapServer = new CoapServer();
			TempResourceHandler tempHandler = new TempResourceHandler();
			_coapServer.add(tempHandler);
		}
		_Logger.info("Starting CoAP server...");
		_coapServer.start();
	}

	/**
	 * Stop the server
	 */
	public void stop()
	{
		_Logger.info("Stopping CoAP server...");
		_coapServer.stop();
	}
}