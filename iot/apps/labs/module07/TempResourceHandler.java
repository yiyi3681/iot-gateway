package apps.labs.module07;

import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

import apps.labs.common.DataUtil;
import apps.labs.common.SensorData;

/**
 * This class inherits CoapResource class and overrides methods to process the requests from the client and send responses
 *
 */
public class TempResourceHandler extends CoapResource{
	//static
	private static final Logger _Logger = 
			Logger.getLogger(TempResourceHandler.class.getName());
	
	/**
	 * Constructor with resource name "temp"
	 */
	public TempResourceHandler() {
		super("temp");
	}

	//public methods
	/**
	 * GET method, this function handles the GET method
	 * @param ce: the CoAP Exchange 
	 */
	@Override
	public void handleGET(CoapExchange ce) 
	{
		String responseMsg = "received GET request";
		ce.respond(ResponseCode.VALID, responseMsg);
		_Logger.info("Received GET request from client.");
	}
	
	/**
	 * POST method, this function handles the POST method
	 * Convert the POST message to SensorData and convert to JSON back
	 * @param ce: the CoAP Exchange 
	 */
	@Override
	public void handlePOST(CoapExchange ce) 
	{
		_Logger.info("JSON data post by client arrived: ");
		_Logger.info(ce.getRequestText());
		DataUtil dataUtil = new DataUtil();
		SensorData sensorData = new SensorData();
		sensorData = dataUtil.jsonToSensorData(ce.getRequestText());
		_Logger.info("Convert JSON data to SensorData: ");
		_Logger.info(sensorData.toString());
		String jsonstr = dataUtil.sensorDataToJson(sensorData);
		_Logger.info("Convert SensorData back to JSON format: ");
		_Logger.info(jsonstr);
		String responseMsg = "received POST request";
		ce.respond(ResponseCode.VALID, responseMsg);
		_Logger.info("Received POST request from client.");
	}
		
	/**
	 * PUT method, this function handles the PUT method
	 * @param ce: the CoAP Exchange 
	 */
	@Override
	public void handlePUT(CoapExchange ce) 
	{
		String responseMsg = "received PUT request";
		ce.respond(ResponseCode.VALID, responseMsg);
		_Logger.info("Received PUT request from client.");
	}
		
	/**
	 * DELETE method, this function handles the DELETE method
	 * @param ce: the CoAP Exchange 
	 */
	@Override
	public void handleDELETE(CoapExchange ce) 
	{
		String responseMsg = "received DELETE request";
		ce.respond(ResponseCode.VALID, responseMsg);
		_Logger.info("Received DELETE request from client.");
	}
}