package apps.labs.module07;

public class CoapClientTestApp {
	/**
	 *This class is used to set up a Coap client and send four kinds of request to the server
	 */
	public static void main(String[] args) {
		CoapClientConnector clientConn = new CoapClientConnector();
		clientConn.runTests("temp");
		}
	}

