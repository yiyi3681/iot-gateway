package apps.labs.module07;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.WebLink;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import apps.labs.common.ConfigConst;
import apps.labs.common.DataUtil;
import apps.labs.common.SensorData;

/**
 *This class realizes the main function of a Coap Client. It initiates a HelperClient and sets up the host and port, 
 *as well as defines the way to process the four kinds of request message and their response.
 */
public class CoapClientConnector {
	// static
	private static final Logger _Logger = Logger.getLogger(CoapClientConnector.class.getName());

	// private var's
	private String _protocol;
	private String _host;
	private int _port;
	private CoapClient _clientConn;
	private String _serverAddr;
	private boolean _isInitialized;

	/**
	 * * Default constructor
	 * 
	 */
	public CoapClientConnector() {
		this(ConfigConst.DEFAULT_COAP_SERVER, false);
	}

	/**
	 * * Constructor with host and port
	 * 
	 * * @param host: The name of the broker to connect.
	 */
	public CoapClientConnector(String host, boolean isSecure) {
		super();
		if (isSecure) {
			_protocol = ConfigConst.SECURE_COAP_PROTOCOL;
			_port = ConfigConst.SECURE_COAP_PORT;
		} else {
			_protocol = ConfigConst.DEFAULT_COAP_PROTOCOL;
			_port = ConfigConst.DEFAULT_COAP_PORT;
		}
		if (host != null && host.trim().length() > 0) {
			_host = host;
		} else {
			_host = ConfigConst.DEFAULT_COAP_SERVER;
		}
		// NOTE: URL does not have a protocol handler for "coap",
		// so we need to construct the URL manually
		_serverAddr = _protocol + "://" + _host + ":" + _port;
		_Logger.info("Using URL for server conn: " + _serverAddr);
	}
	
	/**
	 * Generate a simple SensorData object and initialize its parameters;
	 * Convert the SensorData object to a JSON string.
	 * This is used to test.
	 * @return
	 */
	public String generateMessage() {
		SensorData sensorData = new SensorData();
		DataUtil dataUtil = new DataUtil();
        sensorData.curValue = 20;
        sensorData.minValue = 3;
        sensorData.maxValue = 28;
        sensorData.avgValue = 17;
        sensorData.sampleCount = 6;
        String jsondata = dataUtil.sensorDataToJson(sensorData);
        _Logger.info("Publish JSON string data:");
        _Logger.info(jsondata);
        return jsondata;
	}

	// public methods
	/**
	 * Start the client and implement four requests to the server
	 * POST and PUT the JSON string to the server
	 * @param resourceName: the resource name that client want to connect to
	 */
	public void runTests(String resourceName) {
		try {
			_isInitialized = false;
			initClient(resourceName);
			_Logger.info("Current URI: " + getCurrentUri());
			String payload = this.generateMessage();
			pingServer();
			discoverResources();
			sendGetRequest();
			sendPostRequest(payload, true);
			sendPutRequest(payload, true);
			sendDeleteRequest();
		} catch (Exception e) {
			_Logger.log(Level.SEVERE, "Failed to issue request to CoAP server.", e);
		}
	}
	
	/**
	 * * Returns the CoAP client URI (if set, otherwise returns the _serverAddr, or
	 * * null).
	 * 
	 * * @return String
	 */
	public String getCurrentUri() {
		return (_clientConn != null ? _clientConn.getURI() : _serverAddr);
	}

	/**
	 * Discover web resources
	 */
	public void discoverResources() {
		_Logger.info("Issuing discover...");
		initClient();
		_Logger.info("Getting all remote web likes...");
		Set<WebLink> wlSet = _clientConn.discover();
		if (wlSet != null) {
			for (WebLink wl : wlSet) {
				_Logger.info(" --> WebLink: " + wl.getURI());
			}
		}
	}
	
	/**
	 * Performs a CoAP ping.
	 */
	public void pingServer() {
		initClient();
		if (_clientConn.ping()) {
			_Logger.info("Ping successful!");
		}
	}
	
	/**
	 * Call the correspond methods to process the response message from the server
	 */
	public void sendDeleteRequest() {
		initClient();
		handleDeleteRequest();
	}

	public void sendGetRequest() {
		initClient();
		handleGetRequest();
	}

	public void sendPostRequest(String payload, boolean useCON) {
		initClient();
		handlePostRequest(payload, useCON);
	}

	public void sendPutRequest(String payload, boolean useCON) {
		initClient();
		handlePutRequest(payload, useCON);
	}
		
	// private methods
	/**
	 * Display the response of DELETE request
	 */
	private void handleDeleteRequest() {
		_Logger.info("Sending DELETE...");
		CoapResponse response = _clientConn.delete();
		if(response != null) {
			_Logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		}else {
			_Logger.warning("No response received.");
		}
	}

	/**
	 * Display the response of GET request
	 */
	private void handleGetRequest() {
		_Logger.info("Sending GET...");
		_clientConn.useNONs();
		CoapResponse response = _clientConn.get();
		if(response != null) {
			_Logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		}else {
			_Logger.warning("No response received.");
		}
	}

	/**
	 * Display the  response of PUT request
	 * @param payload
	 * @param useCON
	 */
	private void handlePutRequest(String payload, boolean useCON) {
		_Logger.info("Sending PUT...");
		CoapResponse response = null;
		if (useCON) {
			_clientConn.useCONs().useEarlyNegotiation(32).get();
		}
		response = _clientConn.put(payload, MediaTypeRegistry.TEXT_PLAIN);
		if (response != null) {
			_Logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			_Logger.warning("No response received.");
		}
	}

	/**
	 * Display the  response of POST request
	 * @param payload
	 * @param useCON
	 */
	private void handlePostRequest(String payload, boolean useCON) {
		_Logger.info("Sending POST...");
		CoapResponse response = null;
		if (useCON) {
			_clientConn.useCONs().useEarlyNegotiation(32).get();
		}
		response = _clientConn.post(payload, MediaTypeRegistry.TEXT_PLAIN);
		if (response != null) {
			_Logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			_Logger.warning("No response received.");
		}
	}
	
	/**
	 * Default initiate a client
	 */
	private void initClient() {
		initClient(null);
	}

	/**
	 * Initiate a client and add the resource name to the server address
	 * @param resource: add the resource to the client
	 */
	private void initClient(String resource) {
		if (_isInitialized) {
			return;
		}
		if (_clientConn != null) {
			_clientConn.shutdown();
			_clientConn = null;
		}
		try {
			if (resource != null && resource.trim().length() > 0) {
				_serverAddr += "/" + resource;
			}
			_clientConn = new CoapClient(_serverAddr);
			_Logger.info("Created client connection to server / resource: " + _serverAddr);
		} catch (Exception e) {
			_Logger.log(Level.SEVERE, "Failed to connect to broker: " + getCurrentUri(), e);
		}
	}
}
