package apps.labs.module05;

/**
 * This class is the entrance of module05. 
 * It instantiates a TempSensorAdaptor object to run the tread.
 */
public class TempManagementApp {

	public static void main(String[] args) {
		TempSensorAdaptor tempsensoradaptor = new TempSensorAdaptor();
		tempsensoradaptor.setEnableAdaptorFlag(true);
		tempsensoradaptor.start();
	}

}
