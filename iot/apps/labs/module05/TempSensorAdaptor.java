package apps.labs.module05;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import apps.labs.common.DataUtil;
import apps.labs.common.SensorData;

/**
 * This class is aimed at two things:
 * 1. read the JSON data from file system and write it to a string container
 * 2. use DataUtil object to convert the JSON string to a SensorData object
 */
public class TempSensorAdaptor extends Thread{
	/*
	 * Initialize a tread and a trigger
	 */
	private Thread t;
	boolean enableAdaptor;
	
	/**
	 * Setter of the trigger
	 * @param flag
	 */
	public void setEnableAdaptorFlag(boolean flag) {
		this.enableAdaptor = flag;
	}
	
	/**
	 * Read the file of JSON data by using FileInputStream;
	 * Create a byte vector to write the data in;
	 * Convert the byte buffer to a string.
	 * @return JSON string
	 * @throws IOException
	 */
	public String readString() throws IOException {
		File jsonfile = new File("C:\\Users\\yiyi3\\eclipse-workspace\\iot-device\\sensordata_json.json");//the file path
		InputStream jsondata = new FileInputStream(jsonfile);//instantiate a FileInputStream to read the file
		int size = jsondata.available();//get the size of the file
		byte[] buffer = new byte[size];//create a byte vector as the same size of the file data
		jsondata.read(buffer);//read the file data to the vector
		jsondata.close();//close file
		String str = new String(buffer,"GB2312");//convert the vector to a string
		System.out.println("reading JSON string...");
		System.out.println("JSON string:");
		System.out.println(str);
		return str;
	}
	
	/**
	 * Instantiate a DataUtil object to convert the JSON string to a SensorData object
	 */
	public void run() {
		if(enableAdaptor == true) {
			DataUtil datautil = new DataUtil();
			String str = new String();
			try {
				str = this.readString();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SensorData sensorData = datautil.jsonToSensorData(str);
			System.out.println("\n-------------------");
			System.out.println("New sensor readings:");
			sensorData.showInfo();
		}
	}
	
	/**
	 * Start to implement the thread
	 */
	public void start() {
		System.out.println("TempSensorAdaptor starting...");
		if(t == null) {
			t = new Thread(this);
			t.start();
		}
	}

}
