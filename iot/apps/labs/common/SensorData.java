package apps.labs.common;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class is used to process data from sensor.
 * It can store, update and display temperature information of sensor.
 */
public class SensorData {
	/*
	 * Initialize required parameters
	 */
	public String name = "Temperature";
	public double curValue = 0;
    public double avgValue = 0;
	public double minValue = 0;
    public double maxValue = 0;
    public double totValue = 0;
    public int sampleCount = 0;
	public String timeStamp;
    
	/**
	 * The constructor initializes the current time as timeStamp
	 */
    public SensorData() {
    	Date now = new Date();
    	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    	this.timeStamp = ft.format(now);
    }
    
    /**
     * When new temperature value comes, update the current value, minimum value, maximum value, average value and sample count
     * @param new temperature value
     */
    public void addValue(double newVal) {
    	this.sampleCount = this.sampleCount + 1;
    	if(this.sampleCount == 1) {
    		this.minValue = newVal;
    	}
    	this.totValue = this.totValue + newVal;
    	this.curValue = newVal;
    	if(this.curValue < this.minValue) {
    		this.minValue = this.curValue;
    	}
    	if(this.curValue > this.maxValue) {
    		this.maxValue = this.curValue;
    	}
    	if(this.totValue != 0 && this.sampleCount > 0) {
    		this.avgValue = this.totValue/this.sampleCount;
    	}
    }

    /**
     * getters of parameters
     */
	public String getName() {
		return name;
	}

	public double getValue() {
		return curValue;
	}

	public double getAvgValue() {
		return avgValue;
	}

	public double getMinValue() {
		return minValue;
	}

	public double getMaxValue() {
		return maxValue;
	}
	
	public int getSampleCount() {
		return sampleCount;
	}

	/**
	 * Override toString() method to display the sensor information
	 */
    public String toString() {
    	StringBuilder info = new StringBuilder(" " + this.getName() + ":\n");
    	info.append("\n" + "        Time: " + this.timeStamp + "\n");
    	info.append("\n" + "        Current: " + this.getValue() + "\n");
    	info.append("\n" + "        Average: " + this.getAvgValue() + "\n");
    	info.append("\n" + "        Samples: " + this.getSampleCount() + "\n");
    	info.append("\n" + "        Min: " + this.getMinValue() + "\n");
    	info.append("\n" + "        Max: " + this.getMaxValue() + "\n");
    	return info.toString();
    }
    
    /**
     * Show SensorData as a string
     */
    public void showInfo() {
    	System.out.println(this.toString());
    }
	
    
	

}
