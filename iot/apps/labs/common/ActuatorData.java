package apps.labs.common;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class processes actuator data, it can store, update and display the information from actuator.
 *
 */
public class ActuatorData {
	/*
	 * Initialize required parameters
	 */
    String timeStamp;
    String name = "Temperature";
    boolean hasError = false;
    int command = 0;
    int errCode = 0;
    int statusCode = 0;
    String stateData = null;
    double curValue = 0.0;
    
    /**
     * The constructor initializes the current time as timeStamp
     */
    public ActuatorData() {
    	Date now = new Date();
    	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    	this.timeStamp = ft.format(now);
    }

    /**
     * getters and setters
     */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isHasError() {
		return hasError;
	}

	public int getCommand() {
		return command;
	}

	public void setCommand(int command) {
		this.command = command;
	}

	public int getErrCode() {
		return errCode;
	}

	public void setErrCode(int errCode) {
		this.errCode = errCode;
		if(this.errCode != 0) {
			this.hasError = true;
		}
		else {
			this.hasError = false;
		}
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getStateData() {
		return stateData;
	}

	public void setStateData(String stateData) {
		this.stateData = stateData;
	}

	public double getCurValue() {
		return curValue;
	}

	public void setCurValue(double curValue) {
		this.curValue = curValue;
	}
    
	/**
	 * This method can update the values in ActuatorData
	 */
	public void updateData(ActuatorData actuatorData) {
		this.command = actuatorData.getCommand();
		this.statusCode = actuatorData.getStatusCode();
		this.errCode = actuatorData.getErrCode();
		this.stateData = actuatorData.getStateData();
		this.curValue = actuatorData.getCurValue();
	}
	
	/**
	 *This method can update current time as timeStamp
	 */
	public void updateTimeStamp() {
    	Date now = new Date();
    	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    	this.timeStamp = ft.format(now);
	}
	
	/**
	 * Override toString() method to display the actuator information
	 */
    public String toString() {
    	StringBuilder info = new StringBuilder(" " + this.getName() + ":\n");
    	info.append("\n" + "        Time: " + this.timeStamp + "\n");
    	info.append("\n" + "        Command: " + this.getCommand() + "\n");
    	info.append("\n" + "        Has Error: " + this.isHasError() + "\n");
    	info.append("\n" + "        Status Code: " + this.getStatusCode() + "\n");
    	info.append("\n" + "        Error Code: " + this.getErrCode() + "\n");
    	info.append("\n" + "        State Data: " + this.getStateData() + "\n");
    	info.append("\n" + "        Current Value: " + this.getCurValue());
    	return info.toString();
    }
    
    /**
     * Show the class as string
     */
    public void showInfo() {
    	System.out.println(this.toString());
    }

}
