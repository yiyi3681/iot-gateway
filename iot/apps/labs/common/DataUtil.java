package apps.labs.common;

import com.google.gson.Gson;

/**
 *This class realizes functions of the conversion between two kinds of object(SensorData, ActuatorData) and JSON string
 */
public class DataUtil {
	
	/**
	 * A default constructor
	 */
	public DataUtil() {
		
	}

	/**
	 * This method uses a SensorData object as parameter, 
	 * and encodes it to a JSON string through Gson.toJson()
	 * @param SensorData object
	 * @return JSON string
	 */
	public String sensorDataToJson(SensorData sensorData)
	{
		String jsonData = null;
	    if(sensorData != null) {
	    	Gson gson = new Gson();
	        jsonData = gson.toJson(sensorData);
	        System.out.println("converting SensorData object to JSON string...");
	    }
	    return jsonData;
	}
	
	/**
	 * This method gets a JSON string as a parameter and decodes it to a a SensorData object through Gson.fromJson
	 * @param JSON string
	 * @return SensorData object
	 */
	public SensorData jsonToSensorData(String jsonData) {
		SensorData sensorData = new SensorData();
		if(jsonData != null && jsonData.trim().length() > 0) {
			Gson gson = new Gson();
			sensorData = (SensorData)gson.fromJson(jsonData, SensorData.class);
			System.out.println("converting JSON string to SensorData object...");
		}
		return sensorData;
	}
	
	/**
	 * This method uses a ActuatorData object as parameter, 
	 * and encodes it to a JSON string through Gson.toJson()
	 * @param ActuatorData object
	 * @return JSON string
	 */
	public String actuatorDataToJson(ActuatorData actuatorData) {
		String jsonData = null;
		if(actuatorData != null) {
			Gson gson = new Gson();
			jsonData = gson.toJson(actuatorData);
			System.out.println("converting ActuatorData object to JSON string...");
		}
		return jsonData;
	}
	
	/**
	 * This method gets a JSON string as a parameter and decodes it to a an ActuatorData object through Gson.fromJson
	 * @param JSON string
	 * @return SensorData object
	 */
	public ActuatorData jsonToActuatorData(String jsonData)
	{
		ActuatorData actuatorData = null;
	    if (jsonData != null && jsonData.trim().length() > 0) {
	    	Gson gson = new Gson();
	        actuatorData = (ActuatorData) gson.fromJson(jsonData, ActuatorData.class);
	        System.out.println("converting JSON string to ActuatorData object...");
	 }
	    return actuatorData;
	}


}
