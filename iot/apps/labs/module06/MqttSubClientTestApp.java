package apps.labs.module06;

import java.util.logging.Logger;


/**
 *This class is the mqtt client that subscribe to a topic.
 */
public class MqttSubClientTestApp extends Thread{

	// static Logger
	private static final Logger _Logger = Logger.getLogger(MqttSubClientTestApp.class.getName());
	private static MqttSubClientTestApp _App;
	// parameters
	private MqttClientConnector _mqttClient;

	/**
	 * The main function
	 */
	public static void main(String[] args) {
		_App = new MqttSubClientTestApp();
		try {
			_App.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Default constructor without parameters
	 */

	public MqttSubClientTestApp() {
		super();
	}

	/**
	 * Connect to the mqtt client, and subscribe to the given topic
	 * cancel the subscription after 80s, then disconnect to the mqtt broker
	 */
	public void start() {
		_mqttClient = new MqttClientConnector();
		_mqttClient.connect();
		String topicName = "mqtt-test";
		_mqttClient.subscribeToTopic(topicName); // subscribe to the given topic
		//_mqttClient.subscribeToAll(); // subscribe all the topic
		try {
			sleep(80000);
			_mqttClient.unSubscribeToTopic(topicName);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		_mqttClient.disconnect();
	}

}

