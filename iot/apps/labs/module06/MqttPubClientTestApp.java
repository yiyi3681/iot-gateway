package apps.labs.module06;

import java.util.logging.Logger;

import apps.labs.common.DataUtil;
import apps.labs.common.SensorData;

/**
 *This class is the mqtt client that publishes messages to a topic.
 *It generates a simple SensorData object that can be converted to a JSON string,
 *and publish the string by using MqttClientConnector.
 */
public class MqttPubClientTestApp {
	// static Logger
	private static final Logger _Logger = Logger.getLogger(MqttPubClientTestApp.class.getName());
	private static MqttPubClientTestApp _App;
	
	// parameters
	private MqttClientConnector _mqttClient;
	
	/**
	 * Default constructor without parameters 
	 */
	public MqttPubClientTestApp() {
		super();
	}

	/**
	 * The main function instantiates a new MqttPubClientTestApp object and runs the start()
	 */
	public static void main(String[] args) {
		_App = new MqttPubClientTestApp();
		try {
			_App.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Generate a simple SensorData object and initialize its parameters;
	 * Convert the SensorData object to a JSON string.
	 * This is used to test.
	 * @return
	 */
	public String generateMessage() {
		SensorData sensorData = new SensorData();
		DataUtil dataUtil = new DataUtil();
        sensorData.curValue = 20;
        sensorData.minValue = 3;
        sensorData.maxValue = 28;
        sensorData.avgValue = 17;
        sensorData.sampleCount = 6;
        String jsondata = dataUtil.sensorDataToJson(sensorData);
        _Logger.info("Publish JSON string data:");
        _Logger.info(jsondata);
        return jsondata;
	}
	
	/**
	 * Connect to the mqtt broker, and publish a test message to the given topic
	 */
	public void start() {
		_mqttClient = new MqttClientConnector();
		_mqttClient.connect();
		String topicName = "mqtt-test";
		String payload = this.generateMessage();
		_mqttClient.publishMessage(topicName, 2, payload.getBytes());
		_mqttClient.disconnect();
	}

}
