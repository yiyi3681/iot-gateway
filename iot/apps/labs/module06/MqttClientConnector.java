package apps.labs.module06;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import apps.labs.common.ConfigConst;
import apps.labs.common.DataUtil;
import apps.labs.common.SensorData;

/**
 * This class implements MqttCallback working as a connector to realize mqtt protocol.
 * It contains the methods which can connect and disconnect to a remote host, 
 * publish and subscribe messages, as well as convert the message to JSON or an object.
 *
 */
public class MqttClientConnector implements MqttCallback {

	// static Logger 
	private static final Logger _Logger = Logger.getLogger(MqttClientConnector.class.getName());
	// parameters
	private String _protocol = ConfigConst.DEFAULT_MQTT_PROTOCOL;
	private String _host = ConfigConst.DEFAULT_MQTT_SERVER;
	private int _port = ConfigConst.DEFAULT_MQTT_PORT;
	private String _clientID;
	private String _brokerAddr;
	private MqttClient _mqttClient;

	/**
	 * Default constructor without parameters
	 */
	public MqttClientConnector() {
		this(null);
	}

	/**
	 * Constructor with host name
	 *
	 * @param host: The name of the broker to connect.
	 * @param isSecure Currently unused.
	 */
	public MqttClientConnector(String host) {
		super();
		if (host != null && host.trim().length() > 0) {
			_host = host;
		}
		// NOTE: URL does not have a protocol handler for "tcp",
		// so we need to construct the URL manually
		_clientID = MqttClient.generateClientId();
		_Logger.info("Using client ID for broker conn: " + _clientID);
		_brokerAddr = _protocol + "://" + _host + ":" + _port;
		_Logger.info("Using URL for broker conn: " + _brokerAddr);
	}

	/**
	 * Connect to the remote mqtt broker
	 */
	public void connect() {
		if (_mqttClient == null) {
			MemoryPersistence persistence = new MemoryPersistence();
			try {
				_mqttClient = new MqttClient(_brokerAddr, _clientID, persistence);//set the broker address and client ID in a MqttClient object
				MqttConnectOptions connOpts = new MqttConnectOptions();
				connOpts.setCleanSession(true);
				_mqttClient.setCallback(this);
				_mqttClient.connect(connOpts);//connect to the broker
				_Logger.info("Connected to broker: " + _brokerAddr);
			} catch (MqttException e) {
				_Logger.log(Level.SEVERE, "Failed to connect to broker: " + _brokerAddr, e);//fail to connect
			}
		}
	}

	/**
	 * Disconnect to the remote mqtt broker
	 */
	public void disconnect() {
		try {
			_mqttClient.disconnect();
			_Logger.info("Disconnected from broker: " + _brokerAddr);
		} catch (Exception e) {
			_Logger.log(Level.SEVERE, "Failed to disconnect from broker: " + _brokerAddr, e);
		}
	}

	/**
	 * Publish the given message to broker directly to topic 'topic'.
	 *
	 * @param topic: destination topic that the message direct to
	 * @param qosLevel: 0: at most once, 1: at least once, 2: exactly once
	 * @param payload: the message
	 * @return true if publish successful, false if publish failed
	 */
	public boolean publishMessage(String topic, int qosLevel, byte[] payload) {
		boolean success = false;
		try {
			_Logger.info("Publishing message to topic: " + topic);
			MqttMessage msg = new MqttMessage(payload);//create a new MqttMessage, pass 'payload' to the constructor
			msg.setQos(qosLevel);//set the QoS on the message to qosLevel
			msg.setRetained(true);//Sending a message with retained 
			_mqttClient.publish(topic, msg);//call 'publish' on the MQTT client, passing the 'topic' and MqttMessage instance
			_Logger.info("Published message " + msg.getId() + " to " + topic);//log the result - include the ID from the message
			success = true;
		} catch (Exception e) {
			_Logger.log(Level.SEVERE, "Failed to publish MQTT message: " + e.getMessage());
		}
		return success;
	}

	/**
	 * Subscribe to all topics
	 * @return true if subscribe to all topics successfully, false if failed
	 */
	public boolean subscribeToAll() {
		try {
			_mqttClient.subscribe("$SYS/#");
			_Logger.log(Level.INFO, "Subscribe to all successfully.");
			return true;
		} catch (MqttException e) {
			_Logger.log(Level.WARNING, "Failed to subscribe to all topics.", e);
		}
		return false;
	}

	/**
	 * Subscribe to a certain topic
	 * @param topic: subscribe to the given topic
	 * @return true if subscribe the topic successfully, otherwise false 
	 */
	public boolean subscribeToTopic(String topic) {
		try {			
			_mqttClient.subscribe(topic);
			_Logger.log(Level.INFO, "Subscribe to topic " + topic + " successfully.");
			return true;
		} catch (MqttException e) {
			_Logger.log(Level.WARNING, "Failed to subscribe to topic " + topic, e);
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Cancel the subscription to a certain topic
	 * @param topic: unsubscribe to the input topic
	 * @return true if unsubscribe the topic successfully, otherwise false
	 */
	public boolean unSubscribeToTopic(String topic) {
		try {	
			_mqttClient.unsubscribe(topic);
			_Logger.log(Level.INFO, "Unsubscribe to topic " + topic + " successfully.");
			return true;
		} catch (MqttException e) {
			_Logger.log(Level.WARNING, "Failed to unsubscribe to topic " + topic, e);
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Warn if losing the connection
	 */
	public void connectionLost(Throwable t) {
		_Logger.log(Level.WARNING, "Connection to broker lost. Will retry soon.", t);
		this.connect();
	}

	/**
	 *Show the delivery result in the log
	 */
	public void deliveryComplete(IMqttDeliveryToken token) {
		try {
			_Logger.info("Delivery complete: " + token.getMessageId() + " - " + token.getResponse() + " - "
					+ token.getMessage());
		} catch (Exception e) {
			_Logger.log(Level.SEVERE, "Failed to retrieve message from token.", e);
		}
	}

	/**
	 * Get the message that published from subscribed topic
	 * and convert it to a SensorData object,
	 * then convert to JSON string by using DataUtil
	 */
	public void messageArrived(String data, MqttMessage msg) throws Exception {
		// TODO: should you analyze the message or just log it?
		_Logger.info("Message arrived: " + data + ", " + msg.getId() + ", " + msg.toString());
		DataUtil dataUtil = new DataUtil();
		SensorData sensorData = new SensorData();
		sensorData = dataUtil.jsonToSensorData(msg.toString());
		_Logger.info("convert json message to SensorData:\n" + sensorData.toString());
		String jsonstr = dataUtil.sensorDataToJson(sensorData);
		_Logger.info("convert SensorData back to json message:\n" + jsonstr);
	}

}
